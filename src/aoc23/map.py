import numpy as np

from scipy import signal

SCHEMATIC_SYMBOLS = ['#', '$', '%', '&', '*', '+', '-', '/', '=', '@']
SYMBOL_REMAP_TABLE = str.maketrans({sym:'.' for sym in SCHEMATIC_SYMBOLS})

def map_engine_gears(schematic:np.array):
    
    gear_map = 0
    return False

def get_engine_schematic_ids(schematic:np.array) -> list:
    all_ids = []
    for row in schematic:
        line = ''.join(row)
        line_filt = line.translate(SYMBOL_REMAP_TABLE)
        ids = [int(id) for id in line_filt.split('.') if id]
        all_ids.extend(ids)
        
    return all_ids
    
def map_engine_schematic_ids(schematic:np.array) -> (np.array, np.array):
    # id_map = np.tile(np.nan, schematic.shape)
    id_map = []
    id_count_map = []
    
    id_count_ind = 0
    
    for row in schematic:
        line_orig = ''.join(row)
        line = line_orig.translate(SYMBOL_REMAP_TABLE)
        
        id_strings = [id for id in line.split('.') if id]

        id_map_row = np.tile(np.nan, len(line))
        id_count_row = np.tile(np.nan, len(line))
        

        strt = 0
        for id in id_strings:
            loc = line[strt:].find(id)
            
            siz = len(id)
            _store_at_id_loc(id, id_map_row, strt, loc, siz)
            _store_at_id_loc(id_count_ind, id_count_row, strt, loc, siz)

            strt += loc+siz
            id_count_ind += 1

            
        id_map.append(id_map_row)
        id_count_map.append(id_count_row)
    
    id_map = np.array(id_map)
    id_count_map = np.array(id_count_map)
    return (id_map, id_count_map)

def _store_at_id_loc(id, row, strt, loc, siz):
    row[strt+loc:strt+loc+siz] = id
    
def get_engine_parts(schematic:np.array, 
                     full_output=False) -> list:    
    sym_map = char_map_elem_is_symbol(schematic)
    id_map, id_count_map = map_engine_schematic_ids(schematic)
    
    sym_region = [[1,1,1],[1,1,1],[1,1,1]]
    sym_neigh_map = signal.convolve(sym_map, sym_region, mode='same')
    

    valid_uniq_ids = {
        f"{int(id)}|{int(count)}" 
        for id,count in zip(id_map[(sym_neigh_map>0)],
                            id_count_map[(sym_neigh_map>0)])
        if not np.isnan(id)}
    
    valid_ids = [int(uniq_id.split('|')[0])
                 for uniq_id in valid_uniq_ids]
    
    if full_output:
        valid_uniq_ids = [int(uniq_id.split('|')[1])
                 for uniq_id in valid_uniq_ids]
        return valid_ids, valid_uniq_ids
    
    return valid_ids

def locate_engine_gears(schematic:np.array) -> list:    
    sym_map = char_map_elem_is_symbol(schematic)
    id_map, id_count_map = map_engine_schematic_ids(schematic)
    row_num, col_num = id_map.shape
    
    engine_parts, uniq_part_ids = get_engine_parts(
        schematic, full_output=True)
    
    star_map = char_map_elem_is_star(schematic)
    
    star_row, star_col = np.where(star_map)
    
    gear_uniq_ids = []
    
    gear_info = []
    
    for row,col in zip(star_row, star_col):
        row_lo = np.max((row-1,0))
        row_hi = np.min((row+1,row_num))
        col_lo = np.max((col-1, 0))
        col_hi = np.min((col+1, col_num))
        
        loc_region = id_count_map[row_lo:row_hi+1,col_lo:col_hi+1]
        uniq_neigh_ids = np.unique(loc_region)
        print(loc_region)
        
        neigh_parts_uniq_id = [
            int(uniq_id) for uniq_id in uniq_neigh_ids
            if uniq_id in uniq_part_ids]
        
        
        
        if len(neigh_parts_uniq_id)==2:
            gear_uniq_ids.append(neigh_parts_uniq_id)
            
            
        #     gear_power = 1.0*neigh_parts[0]*neigh_parts[1]
        #     gear_powers.append(gear_power)
            
        print(gear_uniq_ids)
        
        # Now get actual part numbers to get gear ratios

    return None

def char_is_number_or_dot(char) -> bool:
    return char_is_number(char) or char=='.'

def char_is_star(char) -> bool:
    return char == '*'

def char_is_number(char) -> bool:
    return char in ['0','1','2','3','4','5','6','7','8','9']

def char_is_symbol(char) -> bool:
    return not char_is_number_or_dot(char)

char_map_elem_is_number = np.vectorize(char_is_number)
char_map_elem_is_symbol = np.vectorize(char_is_symbol)
char_map_elem_is_star = np.vectorize(char_is_star)
