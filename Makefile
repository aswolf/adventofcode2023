.PHONY: tests devinstall

tests:

ifdef args
	pytest -c tests/pytest.ini $(args)
else
	pytest -c tests/pytest.ini
endif

pyinstall:
	cd src; pip install --upgrade .
	
devinstall:
	pip install -e .