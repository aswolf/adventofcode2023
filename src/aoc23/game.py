import numpy as np
from . import filter

def is_cube_game_possible(game_info:dict, total_rgb:list):    
    max_observed_rgb = game_info['records'].max(axis=0)  
    possible = np.all(max_observed_rgb <= total_rgb)
    return possible

def eval_cube_game_power(game_info:dict):
    records = game_info['records']
    max_observed_rgb = records.max(axis=0)
    return np.product(max_observed_rgb)