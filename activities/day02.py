
from turtle import pos
import numpy as np
import aoc23

datafile = 'data/02_cube_game_records.txt'
with open(datafile, 'r') as file:
    lines = file.readlines()

records = [line.strip('\n\r') for line in lines]

# print(records)

total_rgb = [12, 13, 14]
possible_games = []
power = []

for record in records:
    game_info = aoc23.filter.parse_cube_game_record(record)
    possible = aoc23.game.is_cube_game_possible(game_info, total_rgb)
    if possible:
        possible_games.append(game_info['id'])
        
    power.append(aoc23.game.eval_cube_game_power(game_info))
        

print(f"possible_games = {possible_games}")
print(f"Sum of possible game ids = {sum(possible_games)}")
SUM_POSSIBLE_GAME_IDS = 2283

assert sum(possible_games) == SUM_POSSIBLE_GAME_IDS

print(f"total power = {sum(power)}")
TOTAL_GAME_POWER = 78669

assert sum(power) == TOTAL_GAME_POWER