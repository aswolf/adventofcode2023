import numpy as np

def repair_corrupted_calib_data(corrupted_data:str) -> int:
    corrupted_data_w_digits = _replace_spelled_digits(corrupted_data)
    int_chars = _get_int_chars(corrupted_data_w_digits)
    first_int_char = int_chars[0]
    last_int_char = int_chars[-1]
    calib_val = first_int_char+last_int_char
    return int(calib_val)



def parse_cube_game_record(game_record:str):
    game_info = {}
    
    game_id_str = game_record.split(':')[0]
    game_id = int(game_id_str.split(' ')[-1])
    
    all_records_str = game_record.split(':')[1]
    records = [rec.strip() for rec in all_records_str.split(';')]
    
    record_vals = [_parse_rgb_text_vals(rec) for rec in records]
        
    game_info['id'] = game_id
    game_info['records'] = np.array(record_vals)
    
    return game_info

def _parse_rgb_text_vals(text_record:str):
    rgb = [0,0,0]
    
    color_vals = text_record.split(',')
    for val in color_vals:
        num, color = val.split()
        
        if color=='red':
            rgb[0] = int(num)
            
        elif color=='green':
            rgb[1] = int(num)
            
        elif color=='blue':
            rgb[2] = int(num)
            
        else:
            assert False, 'Color is not valid!!'
            
    return rgb
                
        
    
    
    
def _get_int_chars(corrupted_data:str):
    return [char for char in corrupted_data
            if _is_int_char(char)]
    
def _is_int_char(char):
    return char in '0123456789'

def _replace_spelled_digits(corrupted_data):
    filtered_data = corrupted_data
    replacements = {'one':'one|1|one',
                    'two':'two|2|two',
                    'three':'three|3|three',
                    'four':'four|4|four',
                    'five':'five|5|five',
                    'six':'six|6|six',
                    'seven':'seven|7|seven',
                    'eight':'eight|8|eight',
                    'nine':'nine|9|nine'}
    for name,digit in replacements.items():
        filtered_data = filtered_data.replace(name, digit)
        
    return filtered_data