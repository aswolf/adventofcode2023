import numpy as np
import pandas as pd
from pytest import mark

import aoc23

allclose = np.allclose

def should_succeed():
    assert True
    

@mark.parametrize(
    "corrupt_data,orig_calib_val", 
    [('1abc2',12),('pqr3stu8vwx',38),
     ('a1b2c3d4e5f',15), ('treb7uchet',77)] )
def should_extract_calib_vals(corrupt_data:str, orig_calib_val:int):
    calib_val = aoc23.filter.repair_corrupted_calib_data(corrupt_data)
    assert calib_val == orig_calib_val

@mark.parametrize(
    "corrupt_data,orig_calib_val", 
    [('two1nine',29),('eightwothree',83),
     ('abcone2threexyz',13),('xtwone3four',24),
     ('4nineeightseven2',42),('zoneight234',14),
     ('7pqrstsixteen',76)])
def should_extract_spelled_calib_vals(corrupt_data:str, orig_calib_val:int):
    calib_val = aoc23.filter.repair_corrupted_calib_data(corrupt_data)
    assert calib_val == orig_calib_val


def should_parse_cube_game_record():
    game_record = 'Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red'
    
    game_info = aoc23.filter.parse_cube_game_record(game_record)
    
    assert game_info['id'] ==3
    assert allclose(game_info['records'],[[20,8,6],[4,13,5],[1,5,0]])
    

CUBE_GAME_RECORDS = {
    1:'Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green',
    2:'Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue',
    3:'Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red',
    4:'Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red',
    5:'Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green'}

@mark.parametrize(
    "game_record,is_possible",
    [(CUBE_GAME_RECORDS[1], True),
     (CUBE_GAME_RECORDS[2], True),
     (CUBE_GAME_RECORDS[3],False),
     (CUBE_GAME_RECORDS[4],False),
     (CUBE_GAME_RECORDS[5],True)])
def should_eval_if_cube_game_is_possible(game_record:str, is_possible:bool):
    total_rgb = [12, 13, 14]
    game_info = aoc23.filter.parse_cube_game_record(game_record)
    assert aoc23.game.is_cube_game_possible(game_info, total_rgb) == is_possible
    
@mark.parametrize(
    "game_record,power",
    [(CUBE_GAME_RECORDS[1],48),
     (CUBE_GAME_RECORDS[2],12),
     (CUBE_GAME_RECORDS[3],1560),
     (CUBE_GAME_RECORDS[4],630),
     (CUBE_GAME_RECORDS[5],36)])
def should_eval_cube_game_power(game_record:str, power:bool):
    game_info = aoc23.filter.parse_cube_game_record(game_record)
    assert aoc23.game.eval_cube_game_power(game_info) == power
    
SCHEMATIC_ENGINE_INPUT = {
    'default': [
        '467..114..',
        '...*......',
        '..35..633.',
        '......#...',
        '617*......',
        '.....+.58.',
        '..592.....',
        '......755.',
        '...$.*....',
        '.664.598..',],
    'repeats': [
        '467..114..',
        '...*......',
        '..35..114.',
        '......#...',
        '617*......',
        '.....+.58.',
        '..617.....',
        '......755.',
        '...$.*....',
        '.664.664..',]
}



def should_read_ids_from_engine_schematic():
    engine_schematic = aoc23.convert_input_to_char_map(SCHEMATIC_ENGINE_INPUT['default'])
    schematic_ids = aoc23.map.get_engine_schematic_ids(engine_schematic)
    
    assert set(schematic_ids) == {467, 114, 35, 633, 617, 58, 592, 755, 664, 598}

def should_map_ids_in_engine_schematic():
    engine_schematic = aoc23.convert_input_to_char_map(SCHEMATIC_ENGINE_INPUT['default'])
    
    schematic_ids = aoc23.map.get_engine_schematic_ids(engine_schematic)
    id_map, id_count_map = aoc23.map.map_engine_schematic_ids(engine_schematic)
    
    uniq_count_ind = np.unique(id_count_map)[:-1]
    assert all([id in id_map for id in schematic_ids])
    assert allclose(uniq_count_ind, np.arange(len(uniq_count_ind)))

    

def should_map_symbols_in_engine_schematic():
    engine_schematic = aoc23.convert_input_to_char_map(SCHEMATIC_ENGINE_INPUT['default'])
    sym_map = aoc23.map.char_map_elem_is_symbol(engine_schematic)
    
    assert allclose(1*sym_map, 
            [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
             [0, 0, 0, 1, 0, 0, 0, 0, 0, 0], 
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
             [0, 0, 0, 0, 0, 0, 1, 0, 0, 0], 
             [0, 0, 0, 1, 0, 0, 0, 0, 0, 0], 
             [0, 0, 0, 0, 0, 1, 0, 0, 0, 0], 
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
             [0, 0, 0, 1, 0, 1, 0, 0, 0, 0], 
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])


def sorted_lists_equal(a, b):
    return sorted(a) == sorted(b)

def should_read_part_numbers_from_engine_schematic():
    engine_schematic = aoc23.convert_input_to_char_map(SCHEMATIC_ENGINE_INPUT['default'])

    engine_parts = aoc23.map.get_engine_parts(engine_schematic)
    EXPECTED_PARTS = [467, 35, 633, 617, 592, 755, 664, 598]
    
    assert sorted_lists_equal(engine_parts, EXPECTED_PARTS)
    assert sum(engine_parts) == 4361
    
    
def should_read_part_numbers_from_engine_schematic_with_repeats():
    engine_schematic = aoc23.convert_input_to_char_map(SCHEMATIC_ENGINE_INPUT['repeats'])

    engine_parts = aoc23.map.get_engine_parts(engine_schematic)
    EXPECTED_PARTS = [467, 35, 114, 617, 617, 755, 664, 664]
    
    print(sorted(engine_parts))
    print(sorted(EXPECTED_PARTS))
    assert sorted_lists_equal(engine_parts, EXPECTED_PARTS)
    

@mark.xfail
def should_locate_gears_in_engine_schematic():
    engine_schematic = aoc23.convert_input_to_char_map(
        SCHEMATIC_ENGINE_INPUT['default'])

    gear_map = aoc23.map.locate_engine_gears(engine_schematic)
    
    assert allclose(1*gear_map, 
            [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
             [0, 0, 0, 1, 0, 0, 0, 0, 0, 0], 
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
             [0, 0, 0, 0, 0, 1, 0, 0, 0, 0], 
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])