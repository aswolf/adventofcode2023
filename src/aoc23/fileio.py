import numpy as np

def read_input(filename):
    with open(filename, 'r') as file:
        lines = file.readlines()

    input = [line.strip('\n\r') for line in lines]
    return input

def convert_input_to_char_map(input):
    char_map = np.array([[char for char in line] 
                        for line in input])
    return char_map
