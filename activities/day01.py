
import numpy as np
import aoc23

datafile = 'data/01_trebuchet_calib_file.txt'
with open(datafile, 'r') as file:
    lines = file.readlines()
    
calib_vals = []
for line in lines:
    calib_val = aoc23.filter.repair_corrupted_calib_data(line)
    calib_vals.append(calib_val)
    
calib_vals = np.array(calib_vals)

print(f"Sum of all calib_vals = {sum(calib_vals)}")

PART1_ANSWER = 55538
# assert sum(calib_vals) == PART1_ANSWER

PART2_ANSWER = 54875
assert sum(calib_vals) == PART2_ANSWER

