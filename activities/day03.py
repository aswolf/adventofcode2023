
from turtle import pos
import numpy as np
import aoc23

datafile = 'data/03_engine_schematic.txt'

input = aoc23.read_input(datafile)
schematic = aoc23.convert_input_to_char_map(input)

# print(input)
# print(list(np.unique(schematic.ravel())))

SYMBOLS = ['#', '$', '%', '&', '*', '+', '-', '/', '=', '@']

all_ids = aoc23.map.get_engine_schematic_ids(schematic)

print(f"Num all ids = {len(all_ids)}")
print(f"Num uniq ids = {len(np.unique(all_ids))}")


# id_map = aoc23.map.map_engine_schematic_ids(schematic)
# print(sum(all_ids))
# print(np.nansum(np.unique(id_map.ravel())[:-1]))


engine_parts = aoc23.map.get_engine_parts(schematic)

print(engine_parts)
print(f"sum of all valid engine parts = {sum(engine_parts)}")



# print( 2 in all_ids)
# symbols = ['#' '$' '%' '&' '*' '+' '-' '.' '/' '0' '1' '2' '3' '4' '5' '6' '7' '8'
#  '9' '=' '@']
# print(np.unique(schematic.ravel()))
# print(records)
